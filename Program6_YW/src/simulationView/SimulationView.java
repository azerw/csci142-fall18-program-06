package simulationView;

import java.awt.*;
import simulationController.*;
import simulationModel.*;
import javax.swing.*;
import java.lang.reflect.*;
import java.util.Random;

/**
 * The graph part of the project
 * @author Yizheng Wang
 *
 */
public class SimulationView extends JFrame
{
    // Constants
    private final int    TELLER_WIDTH       = 80;
    private final int    TELLER_HEIGHT      = 85;
    private final String TELLER_IMG         = "images/cashier.png";
    private final int    COUNTER_BOX_WIDTH  = 50;
    private final int    COUNTER_BOX_HEIGHT = 20;
    private final int    CUSTOMER_WIDTH     = 150;
    private final int    CUSTOMER_HEIGHT    = 150;
    private final int    ROW_1              = 650;
    private final int    ROW_2              = 750;
    private final int    MAX_PEOPLE_IN_LINE = 8;
    private final int    MAX_NUM_OF_TELLERS = 5;

    // Data Members
    private SimulationController myController;
    private Container            myContentPane;
    private JLabel[]             myTotalServed;
    private JLabel[]             myNotShown;
    private ButtonListener       myStartPauseListener;
    private ButtonListener[]     myTellerListener;
    private JLabel[][]           myCustomer;
    private JButton              myStartPauseButton;
    private JLabel[]             myTeller;
    private JPanel               mySimPanel;
    private JTextField           numCustomer;
    private JTextField           timeBetween;
    private JTextField           lines;
    private JTextField           serveTime;
    private JTextArea            myOverallStats;
    private JTextArea            myIndiStats;
    private Random               myRandom;

    /**
     * A constructor visualize all the windows, panels and button
     * 
     * @param controller
     */
    public SimulationView(SimulationController controller)
    {
        myRandom = new Random();

        myController = controller;

        myTellerListener = new ButtonListener[5];

        myStartPauseButton = new JButton("Start");
        myStartPauseButton.setSize(50, 25);
        myStartPauseButton.setLocation(75, 775);
        this.associateListeners(myController);

        // Frame
        this.setSize(1080, 848);
        this.setLocation(100, 100);
        this.setTitle("Queue simulator");
        this.setResizable(false);

        myContentPane = getContentPane();
        myContentPane.setLayout(new BorderLayout());

        // Sim Panel
        mySimPanel = new JPanel();
        mySimPanel.setBorder(BorderFactory.createLoweredBevelBorder());
        mySimPanel.setLayout(null);

        // Content boxes
        JPanel myContent = new JPanel();
        myContent.setSize(150, 800);
        myContent.setLayout(new BoxLayout(myContent, BoxLayout.Y_AXIS));
        myOverallStats = new JTextArea(10, 1);
        myIndiStats = new JTextArea(10, 1);
        myIndiStats.setLineWrap(true);
        myIndiStats.setWrapStyleWord(true);
        myIndiStats.setSize(150, 200);

        myOverallStats.setLineWrap(true);
        myOverallStats.setWrapStyleWord(true);
        myOverallStats.setSize(150, 200);

        JPanel myImformation = new JPanel();
        myImformation.setLayout(new GridLayout(4, 2));
        myImformation.setSize(200, 300);
        JLabel myNumCustomer = new JLabel("Number of Customers: ");
        JLabel myTimeBetween = new JLabel("Generate Time:");
        JLabel myLines = new JLabel("Number of Queues: ");
        JLabel myServeTime = new JLabel("Service Time: ");
        numCustomer = new JTextField("0");
        timeBetween = new JTextField("0");
        lines = new JTextField("0");
        serveTime = new JTextField("0");

        myImformation.add(myNumCustomer);
        myImformation.add(numCustomer);
        myImformation.add(myTimeBetween);
        myImformation.add(timeBetween);
        myImformation.add(myLines);
        myImformation.add(lines);
        myImformation.add(myServeTime);
        myImformation.add(serveTime);

        myContent.add(new JLabel("Overall "), BorderLayout.CENTER);
        myContent.add(myOverallStats);
        myContent.add(new JLabel("Individual Cashier"), BorderLayout.CENTER);
        myContent.add(myIndiStats);
        myContent.add(myImformation);
        myContent.add(myStartPauseButton);

        // Customer Served Counter
        myTotalServed = new JLabel[MAX_NUM_OF_TELLERS];

        for (int i = 0; i < MAX_NUM_OF_TELLERS; i++)
        {
            myTotalServed[i] = new JLabel("");
            myTotalServed[i].setSize(COUNTER_BOX_WIDTH, COUNTER_BOX_HEIGHT);
            myTotalServed[i].setLocation(75 + (150 * i), ROW_2);
            myTotalServed[i]
                    .setBorder(BorderFactory.createLineBorder(Color.BLACK));
            mySimPanel.add(myTotalServed[i]);
        }

        // Customer Not Shown Counter
        myNotShown = new JLabel[MAX_NUM_OF_TELLERS];

        for (int i = 0; i < MAX_NUM_OF_TELLERS; i++)
        {
            myNotShown[i] = new JLabel("");
            myNotShown[i].setSize(COUNTER_BOX_WIDTH, COUNTER_BOX_HEIGHT);
            myNotShown[i].setLocation(75 + (150 * i), 10);
            myNotShown[i]
                    .setBorder(BorderFactory.createLineBorder(Color.BLACK));
            mySimPanel.add(myNotShown[i]);

        }

        // Teller locations
        myTeller = new JLabel[MAX_NUM_OF_TELLERS];

        for (int i = 0; i < MAX_NUM_OF_TELLERS; i++)
        {
            myTeller[i] = new JLabel(new ImageIcon(TELLER_IMG));
            myTeller[i].setSize(TELLER_WIDTH, TELLER_HEIGHT);
            myTeller[i].setLocation(60 + (150 * i), ROW_1);
            myTeller[i].setVisible(true);
            mySimPanel.add(myTeller[i]);
        }
        this.associateListeners2(myController);

        // Customer Lines
        myCustomer = new JLabel[MAX_PEOPLE_IN_LINE][MAX_NUM_OF_TELLERS];
        for (int i = 0; i < MAX_NUM_OF_TELLERS; i++)
        {
            for (int j = 0; j < MAX_PEOPLE_IN_LINE; j++)
            {
                myCustomer[j][i] = new JLabel();
                myCustomer[j][i].setSize(CUSTOMER_WIDTH, CUSTOMER_HEIGHT);
                myCustomer[j][i].setLocation(20 + (150 * i), 500 - (60 * j));
                myCustomer[j][i].setVisible(true);
                mySimPanel.add(myCustomer[j][i]);
            }
        }

        // Background
        JLabel bg;
        bg = new JLabel(new ImageIcon("images/bg.jpg"));
        bg.setSize(790, 848);
        bg.setLocation(0, 0);
        mySimPanel.add(bg);
        myContentPane.add(mySimPanel, BorderLayout.CENTER);
        myContentPane.add(myContent, BorderLayout.EAST);
        this.setVisible(true);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

    }

    /**
     * Choose a random customer image
     * 
     * @param num
     * @return customer image
     */
    public Image randomImage(int num)
    {
        Image customer1 = Toolkit.getDefaultToolkit().getImage("images/1.png");
        Image customer2 = Toolkit.getDefaultToolkit().getImage("images/2.png");
        Image customer3 = Toolkit.getDefaultToolkit().getImage("images/3.png");
        Image customer4 = Toolkit.getDefaultToolkit().getImage("images/4.png");
        Image customer5 = Toolkit.getDefaultToolkit().getImage("images/5.png");
        Image customer6 = Toolkit.getDefaultToolkit().getImage("images/6.png");
        Image[] pic = new Image[6];
        pic[0] = customer1.getScaledInstance(CUSTOMER_WIDTH, CUSTOMER_HEIGHT,
                Image.SCALE_SMOOTH);
        pic[1] = customer2.getScaledInstance(CUSTOMER_WIDTH, CUSTOMER_HEIGHT,
                Image.SCALE_SMOOTH);
        pic[2] = customer3.getScaledInstance(CUSTOMER_WIDTH, CUSTOMER_HEIGHT,
                Image.SCALE_SMOOTH);
        pic[3] = customer4.getScaledInstance(CUSTOMER_WIDTH, CUSTOMER_HEIGHT,
                Image.SCALE_SMOOTH);
        pic[4] = customer5.getScaledInstance(CUSTOMER_WIDTH, CUSTOMER_HEIGHT,
                Image.SCALE_SMOOTH);
        pic[5] = customer6.getScaledInstance(CUSTOMER_WIDTH, CUSTOMER_HEIGHT,
                Image.SCALE_SMOOTH);
        return pic[num];
    }

    public JTextField getNumCustomer()
    {
        return numCustomer;
    }

    public JTextField getTimeBetween()
    {
        return timeBetween;
    }

    public JTextField getLines()
    {
        return lines;
    }

    public JTextField getServeTime()
    {
        return serveTime;
    }

    /**
     * Change the text of the button when pressed
     */
    public void changeStartPause()
    {
        if (myStartPauseButton.getText().equals("Start"))
        {
            myStartPauseButton.setText("Pause");
        } else
        {
            myStartPauseButton.setText("Start");
        }
    }

    /**
     * Set customers in the queues
     * 
     * @param queue
     * @param numInLine
     */
    public void setCustomersInLine(int queue, int numInLine)
    {
        myTeller[queue].setIcon(new ImageIcon(TELLER_IMG));

        for (int i = 0; i < MAX_PEOPLE_IN_LINE; i++)
        {
            myCustomer[i][queue].setVisible(false);
        }
        try
        {
            if (numInLine != 0)
            {
                for (int i = 0; i < numInLine && i < MAX_PEOPLE_IN_LINE; i++)
                {
                    myCustomer[i][queue].setVisible(true);
                    myCustomer[i][queue].setIcon(new ImageIcon(
                            this.randomImage(myRandom.nextInt(5))));
                }
            }
        } catch (NullPointerException e)
        {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Set the counter of the customers haven't shown in the window
     * 
     * @param queue
     * @param num
     */
    public void setNotShownCounter(int queue, int num)
    {
        myNotShown[queue].setVisible(true);
        myNotShown[queue].setText("" + num);
    }

    /**
     * Set the counter of the customers have been served
     * 
     * @param queue
     * @param num
     */
    public void setCustomerServedCounter(int queue, int num)
    {
        myTotalServed[queue].setVisible(true);
        myTotalServed[queue].setText("" + num);;
    }

    /**
     * Set the text of the overall statistics of all the service queues
     * 
     * @param aveW
     * @param aveS
     * @param aveI
     * @param totW
     * @param totS
     * @param totI
     * @param totN
     * @param avgN
     */
    public void setOverallStats(float aveW, float aveS, float aveI, float totW,
            float totS, float totI, int totN, int avgN)
    {
        myOverallStats.setText(null);
        myOverallStats.append("Total Seved: " + totN + "\r\n");
        myOverallStats
                .append("Total Sevice Time(s): " + (totS / 1000) + "\r\n");
        myOverallStats
                .append("Total Idle Time(q/s): " + (totI / 1000) + "\r\n");
        myOverallStats
                .append("Total Wait Time(q/s): " + (totW / 1000) + "\r\n");
        myOverallStats.append("\r\n");
        myOverallStats.append("Avg Served: " + avgN + "\r\n");
        myOverallStats
                .append("Avg Sevice Time(q/s): " + (aveS / 1000) + "\r\n");
        myOverallStats.append("Avg Idle Time(q/s): " + (aveI / 1000) + "\r\n");
        myOverallStats.append("Avg Wait Time(q/s): " + (aveW / 1000) + "\r\n");
    }

    /**
     * Set the statistic of the individual queue when clicked on the cashier
     * image
     * 
     * @param avgW
     * @param avgS
     * @param avgI
     * @param num
     * @param q
     */
    public void setIndividualStats(float avgW, float avgS, float avgI, int num,
            int q)
    {
        myIndiStats.setText(null);
        myIndiStats.append("Cashier " + (q + 1) + ": " + "\r\n");
        myIndiStats.append("Total Seved: " + num + "\r\n");
        myIndiStats.append("Avg Service Time(p/s): " + (avgS / 1000) + "\r\n");
        myIndiStats.append("Avg Idle Time(p/s): " + (avgI / 1000) + "\r\n");
        myIndiStats.append("Avg Wait Time(p/s): " + (avgW / 1000) + "\r\n");

    }

    /**
     * Associates each component's listener with the controller and the correct
     * method to invoke when triggered.
     *
     * <pre>
     * pre:  the controller class has be instantiated
     * post: all listeners have been associated to the controller
     *       and the method it must invoke
     * </pre>
     */
    private void associateListeners(SimulationController controller)
    {
        Class<? extends SimulationController> controllerClass;
        Method startPauseMethod;

        controllerClass = myController.getClass();

        startPauseMethod = null;

        try
        {
            startPauseMethod = controllerClass.getMethod("startPause",
                    (Class<?>[]) null);
        } catch (SecurityException e)
        {
            String error;

            error = e.toString();
            System.out.println(error);
        } catch (NoSuchMethodException e)
        {
            String error;

            error = e.toString();
            System.out.println(error);
        }

        myStartPauseListener = new ButtonListener(myController,
                startPauseMethod, null);

        myStartPauseButton.addMouseListener(myStartPauseListener);
    }

    /**
     * Associates each component's listener with the controller and the correct
     * method to invoke when triggered.
     *
     * <pre>
     * pre:  the controller class has be instantiated
     * post: all listeners have been associated to the controller
     *       and the method it must invoke
     * </pre>
     */
    private void associateListeners2(SimulationController controller)
    {
        Class<? extends SimulationController> controllerClass;
        Method disPlayIndiv;
        Class<?>[] classArgs;

        controllerClass = myController.getClass();

        disPlayIndiv = null;
        classArgs = new Class[1];

        try
        {
            classArgs[0] = Class.forName("java.lang.Integer");
        } catch (ClassNotFoundException e)
        {
            String error;
            error = e.toString();
            System.out.println(error);
        }
        try
        {
            disPlayIndiv = controllerClass.getMethod("displayIndivdualStats",
                    classArgs);
        } catch (NoSuchMethodException exception)
        {
            String error;

            error = exception.toString();
            System.out.println(error);
        } catch (SecurityException exception)
        {
            String error;

            error = exception.toString();
            System.out.println(error);
        }

        Integer[] args;

        for (int c = 0; c < MAX_NUM_OF_TELLERS; c++)
        {
            args = new Integer[1];
            args[0] = c;

            myTellerListener[c] = new ButtonListener(myController, disPlayIndiv,
                    args);
            myTeller[c].addMouseListener(myTellerListener[c]);
        }

    }
}
