package simulationController;
import simulationModel.*;
import simulationView.*;

/**
 * The controller of the gui part of the project
 * 
 * @author Yizheng Wang
 *
 */
public class SimulationController implements Runnable
{

    private Thread         myThread;
    private SimulationView myView;
    private boolean        mySuspended;
    private boolean        myBegin;
    private boolean        myEnd;
    private Model          myModel;

    /**
     * Constructor of the controller
     */
    public SimulationController()
    {
        myView = new SimulationView(this);
        myThread = new Thread(this);
        mySuspended = false;
        myBegin = false;
        myEnd = true;
    }

    /**
     * Display the customers in the queue in the view
     * 
     * @param queue
     */
    private void displayCustomers(int queue)
    {
        int numInQueue = myModel.getMyManger().getMyServiceQueues()[queue]
                .getMyNumberCustomersInLine();

        myView.setCustomersInLine(queue, numInQueue);

    }

    /**
     * Display the customerServed counter and the customernotShown counter in
     * the view
     */
    private void displayCounter()
    {
        for (int i = 0; i < myModel.getMyQueueNum(); i++)
        {
            int num = myModel.getMyManger().getMyServiceQueues()[i]
                    .getMyNumberCustomersServedSoFar();
            int num2 = myModel.getMyManger().getMyServiceQueues()[i]
                    .getMyNumberCustomersInLine();
            myView.setCustomerServedCounter(i, num);
            if (num2 > 8)
            {
                myView.setNotShownCounter(i, num2 - 8);
            } else
            {
                myView.setNotShownCounter(i, 0);
            }

        }
    }

    /**
     * Run the thread updating the view
     */
    public void run()
    {
        try
        {
            synchronized (this)
            {
                this.updateView();
            }
        } catch (InterruptedException e)
        {
            System.out.println("Thread suspended.");
        }
    }

    /**
     * Updating the view every 200 ms
     * 
     * @throws InterruptedException
     */
    private void updateView() throws InterruptedException
    {
        while (myEnd)
        {
            this.waitWhileSuspended();
            try
            {
                Thread.sleep(200);

                for (int x = 0; x < myModel.getMyQueueNum(); x++)
                {
                    this.displayCustomers(x);
                    this.displayCounter();
                    this.displayOverallStats();
                    if (myModel.getMyManger().totalServedSoFar() == myModel
                            .getMyCustomerNum())
                    {
                        myEnd = false;
                    }
                }

            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }

    /**
     * Suspend when the condition is true
     * 
     * @throws InterruptedException
     */
    private void waitWhileSuspended() throws InterruptedException
    {
        while (mySuspended)
        {
            this.wait();;
        }
    }

    /**
     * Change the suspended condition
     */
    public void suspend()
    {
        mySuspended = true;
    }

    /**
     * Start the thread
     */
    public void start()
    {
        try
        {
            myThread.start();
        } catch (IllegalThreadStateException e)
        {
            System.out.println("Thread already started");
        }
    }

    /**
     * Resume the thread
     */
    public synchronized void resume()
    {
        mySuspended = false;
        this.notify();

    }

    /**
     * Get the user inputs before start the thread
     */
    private void begin()
    {

        int customer = Integer.parseInt(myView.getNumCustomer().getText());
        int queueNum = Integer.parseInt(myView.getLines().getText());
        int maxServiceTime = Integer.parseInt(myView.getServeTime().getText());
        int maxTimeBetween = Integer
                .parseInt(myView.getTimeBetween().getText());

        myModel = new Model(customer, queueNum, maxServiceTime, maxTimeBetween);
        myBegin = true;
        this.start();

    }

    /**
     * Display the overall statistics of all the service queues
     */
    public void displayOverallStats()
    {
        float aveWait = myModel.getMyManger().averageWaitTime();
        float aveSer = myModel.getMyManger().averageServiceTime();
        float aveIdle = myModel.getMyManger().averageIdleTime();
        float totalWait = myModel.getMyManger().totalWaitTime();
        float totalSer = myModel.getMyManger().totalServiceTime();
        float totalIdle = myModel.getMyManger().totalIdleTime();
        int numCustomer = myModel.getMyManger().totalServedSoFar();
        int avgCustomer = myModel.getMyManger().averageSeved();

        myView.setOverallStats(aveWait, aveSer, aveIdle, totalWait, totalSer,
                totalIdle, numCustomer, avgCustomer);
    }

    /**
     * Displayer the statistics of the individual queue when clicked
     * 
     * @param queue
     */
    public void displayIndivdualStats(Integer queue)
    {
        int q = queue.intValue();
        float avgWait = myModel.getMyManger().getMyServiceQueues()[q]
                .averageWaitTime();
        float avgSer = myModel.getMyManger().getMyServiceQueues()[q]
                .averageServiceTime();
        float avgIdle = myModel.getMyManger().getMyServiceQueues()[q]
                .averageIdleTime();
        int totSeved = myModel.getMyManger().getMyServiceQueues()[q]
                .getMyNumberCustomersServedSoFar();

        myView.setIndividualStats(avgWait, avgSer, avgIdle, totSeved, q);
    }

    /**
     * When the button clicked, change the text of it and do what is told
     */
    public void startPause()
    {

        myView.changeStartPause();
        if (myBegin == false)
        {
            this.begin();
        } else
        {
            if (mySuspended)
            {

                this.resume();
                myModel.resume();
            } else
            {
                this.suspend();
                myModel.pause();
            }
        }
    }

    public static void main(String[] args)
    {
        new SimulationController();
    }
}
