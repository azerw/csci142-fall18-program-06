package simulationModel;

/**
 * An abstract cashier class has an abstract method to generate the service
 * time. Simulate all the cashier activities including serve customers(when
 * there are people in the queue) in random service time and rest every 50 ms if
 * there is no one there.
 * 
 * @author Yizheng Wang
 *
 */
public abstract class Cashier implements Runnable
{
    private int          myMaxTimeOfService;
    private ServiceQueue myQueue;
    private Thread       myThread;
    private boolean      myCondition;
    private boolean      mySuspended;

    /**
     * Constructor of cashier
     * 
     * @param maxTimeSerive
     * @param serviceQueue
     */
    public Cashier(int maxTimeSerive, ServiceQueue serviceQueue)
    {
        myMaxTimeOfService = maxTimeSerive;
        myQueue = serviceQueue;
        myThread = new Thread(this);
        myCondition = false;
        mySuspended = false;

    }

    /**
     * Serve customer when there is one in the queue
     * 
     * @return the customer to be served
     */
    public int serveCustomer()
    {
        return myQueue.getMyNumberCustomersServedSoFar();
    }

    /**
     * Generate the service time in the child class
     * 
     * @return service time in ms
     */
    abstract public int generateServiceTime();

    /**
     * Run the thread that will do "something" of cashier activities
     */
    public void run()
    {
        try
        {

            synchronized (this)
            {
                this.doSomething();
            }
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Start the thread
     */
    public void start()
    {
        try
        {
            myThread.start();
        } catch (IllegalThreadStateException e)
        {
            System.out.println("Thread already started");
        }
    }

    public int getMyMaxTimeOfService()
    {
        return myMaxTimeOfService;
    }

    /**
     * Change the loop condition of cashier(serve customers or keeping checking
     * every 50 ms.
     */
    public void changeCondition()
    {
        myCondition = true;
    }

    /**
     * Change the suspended condition
     */
    public void suspended()
    {
        mySuspended = true;

    }

    /**
     * When suspended is true, suspend the thread
     * 
     * @throws InterruptedException
     */
    private void waitWhileSuspended() throws InterruptedException
    {
        while (mySuspended)
        {
            this.wait();
        }
    }

    /**
     * Cashier activities(loop when condition is true): 
     * 1. Serve customer when there is a one . 
     * 2. If there is no customer, rest for 50 ms and check
     * again.
     * 
     * @throws InterruptedException
     */
    private void doSomething() throws InterruptedException
    {
        try
        {
            int serviceTime;
            int idleTime;
            Long end;
            int start;
            while (myCondition == true)
            {
                this.waitWhileSuspended();

                if (myQueue.getMyNumberCustomersInLine() != 0)
                {
                    serviceTime = this.generateServiceTime();
                    start = myQueue.serveCustomer().getMyEntryTime();
                    end = System.currentTimeMillis();
                    int waitTime = end.intValue() - start;
                    myQueue.addToWaitTime(waitTime);
                    myQueue.addToSeriveTime(serviceTime);
                    Thread.sleep(serviceTime);
                } else
                {
                    idleTime = 50;
                    myQueue.addToIdleTime(idleTime);
                    Thread.sleep(idleTime);
                }
            }

        } catch (InterruptedException e)
        {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Resume the thread
     */
    public synchronized void resume()
    {
        mySuspended = false;
        this.notify();
    }

}
