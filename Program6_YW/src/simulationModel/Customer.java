package simulationModel;

/**
 * Customer class of customer generator
 * @author Yizheng Wang
 *
 */
public class Customer
{
    private int myEntryTime;
    
    /**
     * Constructor of customer
     * @param entryTime
     */
    public Customer(int entryTime) {
        myEntryTime = entryTime;
    }

    public int getMyEntryTime()
    {
        return myEntryTime;
    }
}
