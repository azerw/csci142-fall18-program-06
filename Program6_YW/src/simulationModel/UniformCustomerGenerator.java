package simulationModel;

import java.util.Random;

/**
 * A customer generator generate customers every uniform generating time
 * 
 * @author Yizheng Wang
 *
 */
public class UniformCustomerGenerator extends CustomerGenerator
{
    private Random myRandom;

    /**
     * Constructor
     * @param maxTimeBetweenCustomers
     * @param serviceQueueManager
     * @param numberCustomer
     */
    public UniformCustomerGenerator(int maxTimeBetweenCustomers,
            ServiceQueueManager serviceQueueManager, int numberCustomer)
    {
        super(maxTimeBetweenCustomers, serviceQueueManager, numberCustomer);
    }

    /**
     * Generate uniform generating time
     * @return generating time
     */
    public int generateTimeBetweenCustomers()
    {
        int maxTime = getMyMaxTimeBetweenCustomers();
        myRandom = new Random();
        int timeBetween = myRandom.nextInt(maxTime);
        return timeBetween;
    }

}
