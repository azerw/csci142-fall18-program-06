package simulationModel;

/**
 * A generator generating customers up to specify number 
 * every random generating time
 * 
 * @author Yizheng Wang
 *
 */
public abstract class CustomerGenerator implements Runnable
{
    private int                 myMaxTimeBetweenCustomers;
    private Thread              myThread;
    private int                 myNumberCustomer;
    private ServiceQueueManager myManager;
    private ServiceQueue        myQ;
    private boolean             mySuspended;

    /**
     * Constructor of the generator
     * @param maxTimeBetweenCustomers
     * @param serviceQueueManager
     * @param numberCustomer
     */
    public CustomerGenerator(int maxTimeBetweenCustomers,
            ServiceQueueManager serviceQueueManager, int numberCustomer)
    {
        myMaxTimeBetweenCustomers = maxTimeBetweenCustomers;
        myManager = serviceQueueManager;
        myNumberCustomer = numberCustomer;
        myQ = new ServiceQueue();
        myThread = new Thread(this);
        mySuspended = false;

    }

    /**
     * Generate the generating time between customers
     * @return the generating time
     */
    public abstract int generateTimeBetweenCustomers();

    /**
     * Generate a customer when the time it was generated
     * @return
     */
    public Customer generateCustomer()
    {
        Long begin = System.currentTimeMillis();
        return (new Customer(begin.intValue()));
    }

    /**
     * Run the thread
     */
    public void run()
    {
        try
        {
            synchronized (this)
            {
                this.doSomething();
            }
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }

    }

    public void start()
    {
        try
        {
            myThread.start();
        } catch (IllegalThreadStateException e)
        {
            System.out.println("Thread already started");
        }

    }
    
    public ServiceQueue getMyQ()
    {
        return myQ;
    }


    public int getMyMaxTimeBetweenCustomers()
    {
        return myMaxTimeBetweenCustomers;
    }

    /**
     * Change the suspended condition
     */
    public void suspended()
    {
        mySuspended = true;
    }

    /**
     * Suspend when the condition is true
     * @throws InterruptedException
     */
    private void waitWhileSuspended() throws InterruptedException
    {
        while (mySuspended)
        {
            this.wait();
        }
    }

    /**
     * Generate a customer up to specify number every generating time
     * @throws InterruptedException
     */
    private void doSomething() throws InterruptedException
    {
        try
        {
            int i = 0;
            while (i < myNumberCustomer)
            {
                this.waitWhileSuspended();
                myQ = myManager.determineShortestQueue();
                myQ.insertCustomer(this.generateCustomer());
                Thread.sleep(this.generateTimeBetweenCustomers());;
                i++;
            }
        } catch (InterruptedException e)
        {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Resume the thread
     */
    public synchronized void resume()
    {
        mySuspended = false;
        this.notify();
    }
}
