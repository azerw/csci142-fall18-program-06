package simulationModel;

/**
 * A manager manage all the service queue. Recording all the time of them
 * 
 * @author Yizheng Wang
 *
 */
public class ServiceQueueManager
{
    public static int      MAX_NUMBER_OF_QUEUES;
    private ServiceQueue[] myServiceQueues;
    private float          myTotalWaitTime;
    private float          myTotalServiceTime;
    private float          myTotalIdleTime;
    private float          myAverageServiceTIme;
    private float          myAverageWaitTime;
    private float          myAverageIdleTime;

    /**
     * Constructor of the ServiceQueueManager
     * 
     * @param maxNumber
     */
    public ServiceQueueManager(int maxNumber)
    {
        MAX_NUMBER_OF_QUEUES = maxNumber;
        myServiceQueues = new ServiceQueue[MAX_NUMBER_OF_QUEUES];
        for (int i = 0; i < myServiceQueues.length; i++)
        {
            myServiceQueues[i] = new ServiceQueue();
        }
    }

    /**
     * Get the customers have been served so far
     * 
     * @return the served customers
     */
    public int totalServedSoFar()
    {
        int result = 0;
        for (int i = 0; i < myServiceQueues.length; i++)
        {
            result += myServiceQueues[i].getMyNumberCustomersServedSoFar();
        }
        return result;
    }

    /**
     * Add the wait time of all the service queues
     * 
     * @return the total wait time
     */
    public float totalWaitTime()
    {
        for (int i = 0; i < myServiceQueues.length; i++)
        {
            myTotalWaitTime += myServiceQueues[i].getMyTotalWaitTime();
        }
        return myTotalWaitTime;
    }

    /**
     * Add the service time of all the service queues
     * 
     * @return the total service time
     */
    public float totalServiceTime()
    {
        for (int i = 0; i < myServiceQueues.length; i++)
        {
            myTotalServiceTime += myServiceQueues[i].getMyTotalServiceTime();
        }
        return myTotalServiceTime;
    }

    /**
     * Add the idle time of all the service queues
     * 
     * @return the total idle time
     */
    public float totalIdleTime()
    {
        for (int i = 0; i < myServiceQueues.length; i++)
        {
            myTotalIdleTime += myServiceQueues[i].getMyTotalIdleTime();
        }
        return myTotalIdleTime;
    }

    /**
     * Tell the customer generator which service queue has the least customers
     * 
     * @return
     */
    public ServiceQueue determineShortestQueue()
    {
        int result[] = new int[MAX_NUMBER_OF_QUEUES];
        int min = 1000;
        int index = 0;
        for (int i = 0; i < MAX_NUMBER_OF_QUEUES; i++)
        {
            result[i] = myServiceQueues[i].getSize();
            if (result[i] < min)
            {
                min = result[i];
                index = i;
            }
        }
        return myServiceQueues[index];
    }

    /**
     * Calculate the average wait time of all the service queues
     * @return the average wait time
     */
    public float averageWaitTime()
    {
        myAverageWaitTime = this.totalWaitTime() / MAX_NUMBER_OF_QUEUES;

        return myAverageWaitTime;
    }

    /**
     * Calculate the average service time of all the service queues
     * @return the average service time
     */
    public float averageServiceTime()
    {
        myAverageServiceTIme = this.totalServiceTime() / MAX_NUMBER_OF_QUEUES;
        return myAverageServiceTIme;
    }

    /**
     * Calculate the average idle time of all the service queues
     * @return the average idle time
     */
    public float averageIdleTime()
    {
        myAverageIdleTime = this.totalIdleTime() / MAX_NUMBER_OF_QUEUES;
        return myAverageIdleTime;
    }

    /**
     * Calculate the average customers have been served of all the service queues
     * @return the average served customers
     */
    public int averageSeved()
    {
        return (this.totalServedSoFar() / MAX_NUMBER_OF_QUEUES);
    }

    public ServiceQueue[] getMyServiceQueues()
    {
        return myServiceQueues;
    }

    public float getMyTotalWaitTime()
    {
        return myTotalWaitTime;
    }

    public float getMyTotalServiceTime()
    {
        return myTotalServiceTime;
    }

    public float getMyTotalIdleTime()
    {
        return myTotalIdleTime;
    }

}
