package simulationModel;

/**
 * A model class which creates the Cashier, Customer generator and Service Queue
 * Manger
 * 
 * @author Yizheng Wang
 *
 */
public class Model
{
    private UniformCustomerGenerator myGenerator;
    private ServiceQueueManager      myManger;
    private UniformCashier[]         myCashier;
    private int                      myQueueNum;
    private int                      myCustomerNum;
    private int                      maxTimeBetween;
    private int                      maxServiceTime;

    /**
     * Constructor of Model
     * @param customers
     * @param cashiers
     * @param serviceTime
     * @param generateTime
     */
    public Model(int customers, int cashiers, int serviceTime, int generateTime)
    {
        myQueueNum = cashiers;
        myCustomerNum = customers;
        maxServiceTime = serviceTime;
        maxTimeBetween = generateTime;

        myManger = new ServiceQueueManager(myQueueNum);
        myGenerator = new UniformCustomerGenerator(maxTimeBetween, myManger,
                myCustomerNum);
        myCashier = new UniformCashier[myQueueNum];
        for (int i = 0; i < myQueueNum; i++)
        {
            myCashier[i] = new UniformCashier(maxServiceTime,
                    myManger.getMyServiceQueues()[i]);
        }

        myGenerator.start();
        for (int i = 0; i < myQueueNum; i++)
        {
            myCashier[i].changeCondition();
            myCashier[i].start();
        }
    }

    public int getMyQueueNum()
    {
        return myQueueNum;
    }

    public UniformCustomerGenerator getMyGenerator()
    {
        return myGenerator;
    }

    public ServiceQueueManager getMyManger()
    {
        return myManger;
    }

    /**
     * Pause all the threads
     */
    public void pause()
    {
        myGenerator.suspended();
        for (int i = 0; i < myQueueNum; i++)
        {
            myCashier[i].suspended();;
        }
    }

    /**
     * Resume all the threads
     */
    public void resume()
    {
        myGenerator.resume();
        for (int i = 0; i < myQueueNum; i++)
        {
            myCashier[i].resume();
        }
    }

    public int getMyCustomerNum()
    {
        return myCustomerNum;
    }
}
