package simulationModel;

import java.util.LinkedList;

/**
 * A generic wrapping class of Queue in java, exposing only those methods that
 * should be for a Queue
 * 
 * @author Yizheng Wang
 *
 * @param <T>
 */
public class Queue<T>
{
    private LinkedList<T> myData;
    private int           mySize;

    /**
     * Constructor of a queue
     */
    public Queue()
    {
        myData = new LinkedList<T>();
    }

    /**
     * Add an element to the end of the queue
     * 
     * @param o
     * @return the element to be added
     */
    public boolean enqueue(T o)
    {
        mySize++;
        return myData.offer(o);
    }

    /**
     * Remove the first element of the queue
     * 
     * @return the elements to be removed
     */
    public T dequeue()
    {
        mySize--;
        return myData.poll();
    }

    /**
     * Fetch the first element of the Queue or the element present at the head
     * of the Queue.
     * 
     * @return the elements to be fetched
     */
    public T peek()
    {
        return myData.peek();
    }

    /**
     * Check if the queue is empty
     * @return whether empty or not
     */
    public boolean empty()
    {
        return myData.isEmpty();
    }

    public int length()
    {
        return mySize;
    }

}
