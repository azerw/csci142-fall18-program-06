package simulationModel;

import java.util.Random;

/**
 * A cashier serves customers in uniform random time
 * @author Yizheng Wang
 *
 */
public class UniformCashier extends Cashier
{
    private Random myRandom;

    /**
     * Constructor
     * @param maxTimeSerive
     * @param serviceQueue
     */
    public UniformCashier(int maxTimeSerive, ServiceQueue serviceQueue)
    {
        super(maxTimeSerive,serviceQueue);
    }
    
    /**
     * Generate uniform service time
     * @return the service time
     */
    public int generateServiceTime() {
        int maxTime = getMyMaxTimeOfService();
        myRandom = new Random();
        int serviceTime = myRandom.nextInt(maxTime);
        return serviceTime;
    }
}
