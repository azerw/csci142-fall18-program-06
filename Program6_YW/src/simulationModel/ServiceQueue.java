package simulationModel;

/**
 * Implement the generic queue the cashier served and recording all the time
 * 
 * @author Yizheng Wang
 *
 */
public class ServiceQueue
{
    private int             myNumberCustomersServedSoFar;
    private int             myNumberCustomersInLine;
    private float           myTotalWaitTime;
    private float           myTotalServiceTime;
    private float           myTotalIdleTime;
    private Queue<Customer> myQueue;

    /**
     * Constructor of the service queue
     */
    public ServiceQueue()
    {
        myQueue = new Queue<Customer>();
    }

    /**
     * Add the idle time of the queue
     * @param idle
     */
    public void addToIdleTime(int idle)
    {
        myTotalIdleTime += idle;
    }

    /**
     * Add the wait time of the queue
     * @param wait
     */
    public void addToWaitTime(int wait)
    {
        myTotalWaitTime += wait;
    }

    /**
     * Add the service time of the queue
     * @param service
     */
    public void addToSeriveTime(int service)
    {
        myTotalServiceTime += service;
    }

    /**
     * Insert a customer to the queue
     * @param customer
     */
    public void insertCustomer(Customer customer)
    {
        myQueue.enqueue(customer);
        myNumberCustomersInLine++;
    }

    public Customer serveCustomer()
    {
        myNumberCustomersServedSoFar++;
        myNumberCustomersInLine--;
        return myQueue.dequeue();

    }

    /**
     * Calculate the average wait time of the queue
     * @return the average wait time
     */
    public float averageWaitTime()
    {
        return (myTotalWaitTime / myNumberCustomersServedSoFar);
    }

    /**
     * Calculate the average service time of the queue
     * @return the average service time
     */
    public float averageServiceTime()
    {
        return (myTotalServiceTime / myNumberCustomersServedSoFar);
    }

    /**
     * Calculate the average idle time of the queue
     * @return the average idle time
     */
    public float averageIdleTime()
    {
        return (myTotalIdleTime / myNumberCustomersServedSoFar);
    }

    public float getMyTotalWaitTime()
    {
        return myTotalWaitTime;
    }

    public int getMyNumberCustomersServedSoFar()
    {
        return myNumberCustomersServedSoFar;
    }

    public int getMyNumberCustomersInLine()
    {
        return myNumberCustomersInLine;
    }

    public float getMyTotalServiceTime()
    {
        return myTotalServiceTime;
    }

    public float getMyTotalIdleTime()
    {
        return myTotalIdleTime;
    }

    public Queue<Customer> getMyQueue()
    {
        return myQueue;
    }

    public int getSize()
    {
        return myQueue.length();
    }

}
